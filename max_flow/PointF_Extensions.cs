﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace max_flow
{
    static class PointF_Extensions
    {
        public static PointF Sub(this PointF point, PointF other)
        {
            return new PointF(point.X - other.X, point.Y - other.Y);
        }

        public static PointF Sub(this PointF point, SizeF other)
        {
            return new PointF(point.X - other.Width, point.Y - other.Height);
        }

        public static PointF Sub(this PointF point, float other)
        {
            return new PointF(point.X - other, point.Y - other);
        }

        public static PointF Add(this PointF point, PointF other)
        {
            return new PointF(point.X + other.X, point.Y + other.Y);
        }

        public static PointF Mul(this PointF point, float multiplier)
        {
            return new PointF(point.X * multiplier, point.Y * multiplier);
        }

        public static SizeF Mul(this SizeF point, float multiplier)
        {
            return new SizeF(point.Width * multiplier, point.Height * multiplier);
        }

        public static void Normalize(this PointF point)
        {
            float len = (float)Math.Sqrt(point.X * point.X + point.Y * point.Y);
            point.X = point.X / len;
            point.Y = point.Y / len;
        }

        public static float Dist(this PointF point, PointF other)
        {
            PointF sub = new PointF(point.X - other.X, point.Y - other.Y);
            return (float)Math.Sqrt(sub.X * sub.X + sub.Y * sub.Y);
        }
    
        public static PointF Normalized(this PointF point)
        {
            float len = (float)Math.Sqrt(point.X * point.X + point.Y * point.Y);
            return new PointF(point.X / len, point.Y / len);
        }

        public static T[] Populate<T>(this T[] arr, T value)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = value;
            }
            return arr;
        }

        public static T[,] Populate<T>(this T[,] arr, T value)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    arr[i, j] = value;
                }
            }
            return arr;
        }

        public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
        {
            if (val.CompareTo(min) < 0) return min;
            else if (val.CompareTo(max) > 0) return max;
            else return val;
        }
    }
}
