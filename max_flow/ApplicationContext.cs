﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace max_flow
{
    class ApplicationContext
    {
        private DirectedGraph m_graph;
        private GraphVisualPresentation m_graphVisual;
        private GraphInputController m_inputController;

        public DirectedGraph Graph { get { return m_graph; } }
        public GraphVisualPresentation GraphVisual { get { return m_graphVisual; } }
        public GraphInputController InputController { get { return m_inputController; } }

        public bool HasGraph { get { return m_graph != null; } }

        public void CreateNewGraph(string contents)
        {
            m_graph = DirectedGraph.FromString(contents);
            VisualConfig vconf = new VisualConfig(  minPointDist:0.25f, 
                                                    vertexDiameter:0.1f, 
                                                    arcsThickness:1.0f);
            m_graphVisual = new GraphVisualPresentation(vconf);
            m_inputController = new GraphInputController(m_graphVisual);
            m_graphVisual.Init(m_graph);
        }

        public void ReInitVisual()
        {
            if (HasGraph)
            {
                m_graphVisual.Init(m_graph);
            }
        }

        public void Draw(Graphics graphics)
        {
            if (HasGraph)
            {
                m_graphVisual.PaintGraph(graphics);
            }
        }

        public void SetCanvasBounds(RectangleF bounds)
        {
            if(m_graphVisual != null)
            {
                m_graphVisual.SetCanvasSize(bounds);
            }
            if(m_inputController != null)
            {
                m_inputController.SetCanvasSize(bounds);
            }
        }
    }
}
