﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace max_flow
{
    class DirectedGraph : Graph
    {
        public int NumOfArcs { get; private set; }

        public static DirectedGraph FromString(string contents)
        {
            DirectedGraph g = new DirectedGraph();
            string[] strRows = contents.Split('\n');

            int numOfVerts = strRows.Length;
            g.NumOfVertices = numOfVerts;
            g.weights = new int[numOfVerts][];
            for(int row = 0; row < numOfVerts; row++)
            {
                g.weights[row] = new int[numOfVerts];
                string[] strColumns = strRows[row].Split(new char[] { ' ', ',', '.', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                for (int column = 0; column < numOfVerts; column++)
                {
                    int newWeight = k_infiniteWeight;
                    if (column < strColumns.Length)
                    {
                        if (!int.TryParse(strColumns[column], out newWeight))
                        {
                            newWeight = k_infiniteWeight;
                        }
                    }
                    g.weights[row][column] = newWeight;
                }
            }
            return g;
        }
    }
}
