﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace max_flow
{
    enum PointerEvents
    {
        Hold,
        Release
    }

    class GraphInputController
    {
        private GraphVisualPresentation m_visual;

        int     m_heldVertice;

        RectangleF m_canvasDimensions;

        public GraphInputController(GraphVisualPresentation visual)
        {
            m_visual = visual;
            m_heldVertice = -1;
            m_canvasDimensions = new RectangleF(0.0f, 0.0f, 1.0f, 1.0f);
        }

        public void Clear()
        {
            m_heldVertice = -1;
            m_visual.ClearVertexSelection();
        }

        public void SetCanvasSize(RectangleF canvasDimensions)
        {
            m_canvasDimensions = canvasDimensions;
        }

        public void TrackPointer(PointF pos)
        {
            int vertexUnderPointer = m_visual.QueryVertex(pos);
            if(vertexUnderPointer == -1)
            {
                m_visual.ClearVertexSelection();
            }
            else 
            {
                if (m_heldVertice == -1)
                {
                    m_visual.SelectVertex(vertexUnderPointer, SelectionType.PointerHover);
                }
            }

            if(m_heldVertice != -1)
            {
                m_visual.SetVertexPosition(m_heldVertice, pos);
            }
        }

        public void PointerEvent(PointerEvents pevent, PointF pos)
        {
            if (pevent == PointerEvents.Hold)
            {
                PointF difference;
                int vertexUnderPointer = m_visual.QueryVertex(pos);
                if (vertexUnderPointer != -1)
                {
                    m_heldVertice = vertexUnderPointer;
                    m_visual.SelectVertex(vertexUnderPointer, SelectionType.PointerHold);
                }
            }
            else
            {
                m_heldVertice = -1;
            }
        }

        void Init(GraphVisualPresentation visual)
        {

        }
    }
}
