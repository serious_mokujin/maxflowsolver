﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace max_flow
{
    struct VisualConfig
    {
        public VisualConfig(float minPointDist, float vertexDiameter, float arcsThickness)
        {
            this.minPointDistance = minPointDist;
            this.vertexDiameter = vertexDiameter;
            this.arcsThickness = arcsThickness;
        }

        public float minPointDistance;
        public float vertexDiameter;
        public float arcsThickness;
    }

    enum SelectionType
    {
        None,
        PointerHover,
        PointerHold
    }

    struct VertexVisualStyle
    {
        public Font vertexNumberTextFont;
        public Brush vertexNumberTextBrush;
        public Pen vertexOutlinePen;
    }

    struct ArcVisualStyle
    {
        public Font arcWeightTextFont;
        public Brush arcWeightTextBrush;
        public Pen oneWayArcPen;
        public Pen twoWayArcPen;
    }

    struct VisualStyleCollection
    {
        public VertexVisualStyle regularVertexStyle;
        public VertexVisualStyle hoverVertexStyle;
        public VertexVisualStyle holdenVertexStyle;

        public ArcVisualStyle regularArcVisualStyle;
        public ArcVisualStyle selectedArcVisualStyle;
    }

    class GraphVisualPresentation
    {
        private PointF[] m_pointPositions;
        private VisualConfig m_config;
        private Graph m_graph;

        int m_selectedVertex;
        SelectionType m_vertexSelectionType;

        public event Action OnVisualChange;

        RectangleF m_canvasDimensions;

        VisualStyleCollection m_visualStyleCollection;

        Solution m_solution;
        int solutionStep;

        public GraphVisualPresentation(VisualConfig config)
        {
            m_config = config;
            m_selectedVertex = -1;
            m_vertexSelectionType = SelectionType.None;
            m_canvasDimensions = new RectangleF(0.0f, 0.0f, 1.0f, 1.0f);
            m_solution = null;
            solutionStep = 0;
            CreateVisualStyleCollection();
        }

        private void CreateVisualStyleCollection()
        {
            Font arialFontBig = new Font("Arial", CanvasUpScale * m_config.vertexDiameter * .5f);
            Font arialFontSmall = new Font("Arial", CanvasUpScale * .03f);

            m_visualStyleCollection.regularVertexStyle.vertexNumberTextFont = arialFontBig;
            m_visualStyleCollection.regularVertexStyle.vertexNumberTextBrush = Brushes.Black;
            m_visualStyleCollection.regularVertexStyle.vertexOutlinePen = Pens.Black;

            m_visualStyleCollection.holdenVertexStyle.vertexNumberTextFont = arialFontBig;
            m_visualStyleCollection.holdenVertexStyle.vertexNumberTextBrush = Brushes.Green;
            m_visualStyleCollection.holdenVertexStyle.vertexOutlinePen = Pens.Green;

            m_visualStyleCollection.hoverVertexStyle.vertexNumberTextFont = arialFontBig;
            m_visualStyleCollection.hoverVertexStyle.vertexNumberTextBrush = Brushes.Red;
            m_visualStyleCollection.hoverVertexStyle.vertexOutlinePen = Pens.Red;

            {
                float arrowSize = m_config.arcsThickness * CanvasUpScale * 0.02f;
                AdjustableArrowCap bigArrow = new AdjustableArrowCap(arrowSize, arrowSize);

                {
                    Pen oneWayArrow = new Pen(Color.Black, m_config.arcsThickness);
                    oneWayArrow.StartCap = System.Drawing.Drawing2D.LineCap.NoAnchor;
                    oneWayArrow.CustomEndCap = bigArrow;

                    Pen twoWayArrow = new Pen(Color.Black, m_config.arcsThickness);
                    twoWayArrow.CustomStartCap = bigArrow;
                    twoWayArrow.CustomEndCap = bigArrow;

                    m_visualStyleCollection.regularArcVisualStyle.arcWeightTextFont = arialFontSmall;
                    m_visualStyleCollection.regularArcVisualStyle.arcWeightTextBrush = Brushes.Black;
                    m_visualStyleCollection.regularArcVisualStyle.oneWayArcPen = oneWayArrow;
                    m_visualStyleCollection.regularArcVisualStyle.twoWayArcPen = twoWayArrow;
                }
                {
                    Pen oneWayArrow = new Pen(Color.Blue, m_config.arcsThickness * 2f);
                    oneWayArrow.StartCap = System.Drawing.Drawing2D.LineCap.NoAnchor;
                    oneWayArrow.CustomEndCap = bigArrow;

                    Pen twoWayArrow = new Pen(Color.Blue, m_config.arcsThickness * 2f);
                    twoWayArrow.CustomStartCap = bigArrow;
                    twoWayArrow.CustomEndCap = bigArrow;

                    m_visualStyleCollection.selectedArcVisualStyle.arcWeightTextFont = arialFontSmall;
                    m_visualStyleCollection.selectedArcVisualStyle.arcWeightTextBrush = Brushes.Red;
                    m_visualStyleCollection.selectedArcVisualStyle.oneWayArcPen = oneWayArrow;
                    m_visualStyleCollection.selectedArcVisualStyle.twoWayArcPen = twoWayArrow;
                }
            }
        }

        internal void SetSolution(Solution solution)
        {
            m_solution = solution;
            solutionStep = 0;
            ReportVisualChange();
        }

        internal void SelectSolutionStep(int step)
        {
            solutionStep = step;
            ReportVisualChange();
        }

        public void Init(Graph graph)
        {
            m_graph = graph;
            m_pointPositions = new PointF[graph.NumOfVertices];
            Random rnd = new Random();
            for (int vert = 0; vert < graph.NumOfVertices; vert++)
            {
                RectangleF allowedArea = new RectangleF(0.0f, 0.0f, 1.0f, 1.0f);
                allowedArea.Inflate(-m_config.vertexDiameter, -m_config.vertexDiameter);
                PointF newPos = new PointF();
                bool positionIsOk = true;
                do {
                    positionIsOk = true;
                    newPos = new PointF(allowedArea.X + (float)rnd.NextDouble() * allowedArea.Width,
                                                        allowedArea.Y + (float)rnd.NextDouble() * allowedArea.Height);
                    for (int i = 0; i < vert; i++)
                    {
                        if(newPos.Dist(m_pointPositions[i]) < m_config.minPointDistance)
                        {
                            positionIsOk = false;
                            break;
                        }
                    }
                } while (!positionIsOk);
                m_pointPositions[vert] = newPos;
            }
            ReportVisualChange();
        }

        public void SetCanvasSize(RectangleF canvasDimensions)
        {
            m_canvasDimensions = canvasDimensions;
            CreateVisualStyleCollection();
            ReportVisualChange();
        }

        private float CanvasUpScale
        {
            get { return Math.Min(m_canvasDimensions.Width, m_canvasDimensions.Height); }
        }

        private void ReportVisualChange()
        {
            if (OnVisualChange != null)
            {
                OnVisualChange();
            }
        }

        public PointF GetVertexPosition(int vertexIndex)
        {
            return TransformToCanvasSpace(m_pointPositions[vertexIndex], m_canvasDimensions);
        }

        public void SetVertexPosition(int vertexIndex, PointF position)
        {
            m_pointPositions[vertexIndex] = TransformToInternalSpace(position, m_canvasDimensions);
            ReportVisualChange();
        }

        private static PointF TransformToInternalSpace(PointF canvasSpace, RectangleF canvasDimensions)
        {
            float scale = Math.Min(canvasDimensions.Width, canvasDimensions.Height);
            PointF offset = new PointF((canvasDimensions.Width - scale) * .5f, (canvasDimensions.Height - scale) * .5f);
            PointF vpos = canvasSpace.Sub(offset).Mul(1.0f / scale);
            return vpos;
        }

        private static PointF TransformToCanvasSpace(PointF internalSpace, RectangleF canvasDimensions)
        {
            float scale = Math.Min(canvasDimensions.Width, canvasDimensions.Height);
            PointF offset = new PointF((canvasDimensions.Width - scale) * .5f, (canvasDimensions.Height - scale) * .5f);
            PointF vpos = offset.Add(internalSpace.Mul(scale));
            return vpos;
        }

        public void SelectVertex(int vertex, SelectionType selectionType)
        {
            if (m_selectedVertex != vertex || m_vertexSelectionType != selectionType)
            {
                m_selectedVertex = vertex;
                m_vertexSelectionType = selectionType;
                ReportVisualChange();
            }
        }
        public void ClearVertexSelection()
        {
            if (m_vertexSelectionType != SelectionType.None)
            {
                m_vertexSelectionType = SelectionType.None;
                ReportVisualChange();
            }
        }

        public int QueryVertex(PointF pointer)
        {
            for(int vert = 0; vert < m_pointPositions.Length; vert++)
            {
                PointF vpos = TransformToCanvasSpace(m_pointPositions[vert], m_canvasDimensions);
                if(vpos.Dist(pointer) < m_config.vertexDiameter * .5f * CanvasUpScale)
                {
                    return vert;
                }
            }
            return -1;
        }

        private VertexVisualStyle GetVertexVisualStyle(int vertex)
        {
            if (m_selectedVertex == vertex)
            {
                if (m_vertexSelectionType == SelectionType.PointerHover)
                {
                    return m_visualStyleCollection.hoverVertexStyle;
                }
                else if (m_vertexSelectionType == SelectionType.PointerHold)
                {
                    return m_visualStyleCollection.holdenVertexStyle;
                }
            }
            return m_visualStyleCollection.regularVertexStyle;
        }

        private ArcVisualStyle GetArcVisualStyle(int v1, int v2)
        {
            if(m_solution != null)
            {
                StepSlice step = m_solution.m_steps[solutionStep];
                if(step.action == StepAction.TraceBackStep)
                {
                    if(step.flowChange.arcV1 == v1 && step.flowChange.arcV2 == v2)
                    {
                        return m_visualStyleCollection.selectedArcVisualStyle;
                    }
                }
                else if(step.action == StepAction.FindOutMinimalCut)
                {
                    for (int i = 0; i < m_solution.min_cut.Length; i++)
                    {
                        if(v1 == m_solution.min_cut[i].Item1 && v2 == m_solution.min_cut[i].Item2)
                        {
                            return m_visualStyleCollection.selectedArcVisualStyle;
                        }
                    }
                }
            }
            return m_visualStyleCollection.regularArcVisualStyle;
        }

        public void PaintGraph(Graphics graphics)
        {
            DrawArcs(graphics);
            DrawVertices(graphics);
        }

        private void DrawVertices(Graphics g)
        {
            for (int vindex = 0; vindex < m_pointPositions.Length; vindex++)
            {
                VertexVisualStyle style = GetVertexVisualStyle(vindex);
                PointF vpos = m_pointPositions[vindex];
                RectangleF circleBounds = new RectangleF(
                    TransformToCanvasSpace(vpos.Sub(m_config.vertexDiameter * .5f), m_canvasDimensions), 
                    new SizeF(m_config.vertexDiameter * CanvasUpScale, m_config.vertexDiameter * CanvasUpScale));
                if(m_solution != null)
                {
                    StepSlice currentStep = m_solution.m_steps[solutionStep];
                    if(m_solution.S == vindex)
                    {
                        g.FillEllipse(Brushes.LightBlue, circleBounds);
                    }
                    else if(m_solution.T == vindex)
                    {
                        g.FillEllipse(Brushes.IndianRed, circleBounds);
                    }
                    if(currentStep.marks_state[vindex].IsValid())
                    {
                        RectangleF markedOutline = circleBounds;
                        markedOutline.Inflate(-circleBounds.Width * 0.1f, -circleBounds.Height * 0.1f);
                        g.DrawEllipse(new Pen(Color.Black, 3), markedOutline);
                    }
                }
                g.DrawEllipse(style.vertexOutlinePen, circleBounds);

                // draw number
                string vertexNumberStr = (vindex + 1).ToString();
                SizeF strSize = g.MeasureString(vertexNumberStr, style.vertexNumberTextFont);
                PointF strPos = TransformToCanvasSpace(vpos, m_canvasDimensions).Sub(strSize.Mul(.5f)); 
                g.DrawString(vertexNumberStr, style.vertexNumberTextFont, style.vertexNumberTextBrush, strPos);
            }
        }

        private void DrawArcs(Graphics g)
        {
            for (int row = 0; row < m_graph.NumOfVertices; row++)
            {
                for(int col = 0; col < row; col++)
                {
                    ArcVisualStyle style = GetArcVisualStyle(row, col);
                    int weightFromP1 = m_graph[row, col];
                    int weightFromP2 = m_graph[col, row];

                    int ways = 0;
                    ways += m_graph[row, col] != Graph.k_infiniteWeight ? 1 : 0;
                    ways += m_graph[col, row] != Graph.k_infiniteWeight ? 1 : 0;

                    PointF p1 = TransformToCanvasSpace(m_pointPositions[row], m_canvasDimensions);
                    PointF p2 = TransformToCanvasSpace(m_pointPositions[col], m_canvasDimensions);

                    if (weightFromP1 != Graph.k_infiniteWeight || weightFromP2 != Graph.k_infiniteWeight)
                    {
                        if (weightFromP2 != Graph.k_infiniteWeight && weightFromP1 == Graph.k_infiniteWeight)
                        {
                            PointF tmp = p1;
                            p1 = p2;
                            p2 = tmp;

                            style = GetArcVisualStyle(col, row);
                        }
                        
                        PointF direction = p2.Sub(p1).Normalized();
                        PointF additionalOffset = direction.Mul(CanvasUpScale * m_config.vertexDiameter * .5f);

                        PointF finalP1 = additionalOffset.Add(p1);
                        PointF finalP2 = p2.Sub(additionalOffset);
                        g.DrawLine(ways == 2 ? style.twoWayArcPen : style.oneWayArcPen, finalP1, finalP2);
                        
                        PointF middlePointP1 = finalP1.Add(finalP2.Sub(finalP1).Mul(.4f));
                        PointF middlePointP2 = finalP1.Add(finalP2.Sub(finalP1).Mul(.6f));
                        DrawWeightText(g, style, middlePointP1, row, col);
                        DrawWeightText(g, style, middlePointP2, col, row);
                    }
                }
            }
        }

        void DrawWeightText(Graphics g, ArcVisualStyle style, PointF pos, int v1, int v2 )
        {
            int weight = m_graph[v1, v2];
            if (weight != Graph.k_infiniteWeight)
            {
                string weightStr = weight.ToString();

                if(m_solution != null)
                {
                    StepSlice step = m_solution.m_steps[solutionStep];
                    int flow = step.flow_state[v1, v2];
                    weightStr += string.Format("({0})", flow);
                }

                SizeF strSize = g.MeasureString(weightStr, style.arcWeightTextFont);
                PointF strPos = pos.Sub(strSize.Mul(.5f));
                RectangleF background = new RectangleF(pos, strSize);
                background.Offset(-strSize.Width * .5f, -strSize.Height * .5f);
                g.FillRectangle(Brushes.White, background);
                g.DrawString(weightStr, style.arcWeightTextFont, style.arcWeightTextBrush, strPos);
            }
        }
    }
}
