﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace max_flow
{
    public partial class Form1 : Form
    {
        ApplicationContext m_context;
        SolutionExplorer solutionExplorer = null;

        public Form1()
        {
            m_context = new ApplicationContext();
            
            ////// temp
            string fileContents = File.ReadAllText("../../data/test_1.txt");
            m_context.CreateNewGraph(fileContents);
            m_context.GraphVisual.OnVisualChange += GraphVisual_OnVisualChange;
            ///////////

            InitializeComponent();

            m_context.SetCanvasBounds(new Rectangle(canvasPlaceholder.Location, canvasPlaceholder.Size));
        }

        private void GraphVisual_OnVisualChange()
        {
            Invalidate();
        }

        void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }

        void Form1_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if(files.Length > 0)
            {
                string fileContents = File.ReadAllText(files[0]);
                m_context.CreateNewGraph(fileContents);
                m_context.GraphVisual.OnVisualChange += GraphVisual_OnVisualChange;
                this.Invalidate();
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            m_context.Draw(e.Graphics);
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            m_context.SetCanvasBounds(new Rectangle(canvasPlaceholder.Location, canvasPlaceholder.Size));
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                m_context.ReInitVisual();
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            m_context.InputController.TrackPointer(new PointF(e.X, e.Y));
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            m_context.InputController.PointerEvent(PointerEvents.Hold, new PointF(e.X, e.Y));
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            m_context.InputController.PointerEvent(PointerEvents.Release, new PointF(e.X, e.Y));
        }

        private void Form1_MouseEnter(object sender, EventArgs e)
        {
        }

        private void Form1_MouseLeave(object sender, EventArgs e)
        {
            m_context.InputController.Clear();
        }

        private void найтиМаксимальныйПотокToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MaxFlowSolver maxFlowSolver = new MaxFlowSolver(m_context.Graph);
            SolutionResult result = maxFlowSolver.Solve();
            if (result == SolutionResult.Success)
            {
                Solution solution = maxFlowSolver.Solution;
                m_context.GraphVisual.SetSolution(solution);
                solutionExplorer = new SolutionExplorer(solution);
                solutionExplorer.OnStepChanged += SolutionExplorer_OnStepChanged;
                solutionExplorer.OnClosed += SolutionExplorer_OnClosed;
                solutionExplorer.Show();
            }
            else
            {
                MessageBox.Show("Произошла ошибка во время решения графа");
            }
        }

        private void SolutionExplorer_OnClosed()
        {
            m_context.GraphVisual.SetSolution(null);
        }

        private void SolutionExplorer_OnStepChanged(int step)
        {
            m_context.GraphVisual.SelectSolutionStep(step);
        }
    }
}
