﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace max_flow
{
    public partial class SolutionExplorer : Form
    {
        int currentSolutionStep;
        Solution m_solution;

        public event Action<int> OnStepChanged;
        public event Action OnClosed;

        public SolutionExplorer(Solution solution)
        {
            m_solution = solution;

            InitializeComponent();
            trackBar1.Maximum = solution.m_steps.Count - 1;
            trackBar1.Value = 0;
            GoToStep(0);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int nextStep = Math.Max(0, currentSolutionStep - 1);
            trackBar1.Value = nextStep;
            GoToStep(nextStep);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int nextStep = Math.Min(currentSolutionStep + 1, m_solution.m_steps.Count - 1);
            trackBar1.Value = nextStep;
            GoToStep(nextStep);
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            GoToStep(trackBar1.Value);
        }

        private void GoToStep(int step)
        {
            currentSolutionStep = step.Clamp(0, m_solution.m_steps.Count - 1);
            textBox1.Text = "";
            textBox1.Text += Environment.NewLine + PrintStepDescription(currentSolutionStep, m_solution.m_steps[currentSolutionStep]);
            textBox1.Text += Environment.NewLine + PrintMarks(m_solution.m_steps[currentSolutionStep].marks_state);
            if(OnStepChanged != null)
            {
                OnStepChanged(currentSolutionStep);
            }
        }

        string PrintStepDescription(int step, StepSlice slice)
        {
            string d = string.Format("Шаг {0}. ", step + 1);
            string stepName = "";
            string stepDescription = "";
            switch (slice.action)
            {
                case StepAction.Initialization:
                    stepName = "Инициализация.";
                    stepDescription = "Нахождение истока S и стока T.";
                    break;
                case StepAction.StartMarking:
                    stepName = "Начало помечивания вершин.";
                    stepDescription = "Помечаем исток как (-,∞) и начинаем обход по смежным вершинам.";
                    break;
                case StepAction.MarkVertice:
                    stepName = "Пометка вершины.";
                    stepDescription = string.Format("Помечена вершина {0}.", slice.vertexMark.justMarkedVertice + 1);
                    break;
                case StepAction.StockMarked:
                    stepName = "Помечен сток.";
                    stepDescription = string.Format("Помечена вершина {0}.", slice.vertexMark.justMarkedVertice + 1);
                    break;
                case StepAction.StartTraceBack:
                    stepName = "Начало обратной трассировки.";
                    stepDescription = "Далее будет произведено изменение потока";
                    break;
                case StepAction.TraceBackStep:
                    stepName = "Перераспределение потока.";
                    stepDescription = string.Format("Для дуги {0}-{1} поток изменился с {2} на {3}.", slice.flowChange.arcV1 + 1, slice.flowChange.arcV2 + 1, slice.flowChange.oldFlow, slice.flowChange.newFlow);
                    break;
                case StepAction.TraceBackFinish:
                    stepName = "Исток найден, окончание трасскировки.";
                    stepDescription = "Потоки были изменены";
                    break;
                case StepAction.FindOutMinimalCut:
                    stepName = "Сток небыл помечен. Находим минимальный разрез.";
                    stepDescription = string.Format("Максимальный поток равен: {0}", m_solution.maxFlow);
                    break;
                default:
                    break;
            }
            d += stepName + Environment.NewLine + stepDescription;
            return d;
        }

        string PrintMarks(VertexMark[] marks)
        {
            string s = "";
            string s1 = "┌───────┬";
            string s2 = "│вершина│";
            string s3 = "├───────┼";
            string s4 = "│пометка│";
            string s5 = "└───────┴";
            int colSize = 8;
            for(int i = 0; i < marks.Length; i++)
            {
                string markString = " ";
                if (marks[i].IsValid())
                {
                    string mark = (marks[i].sign > 0 ? "+" : "-") + marks[i].mark.ToString();
                    if (marks[i].mark == -1)
                    {
                        markString = "(-,∞)";
                    }
                    else
                    {
                        markString = string.Format("({0},{1})", mark, marks[i].flowDif);
                    }
                }

                bool isLast = i == (marks.Length - 1);
                s1 += "─".PadRight(colSize, '─') + (isLast ? "┐" : "┬");
                s2 += string.Format("{0,-"+ colSize.ToString()+ "}{1}", i + 1, "│");
                s3 += "─".PadRight(colSize, '─') + (isLast ? "┤" : "┼");
                s4 += string.Format("{0,-" + colSize.ToString() + "}", markString) + "│";
                s5 += "─".PadRight(colSize, '─') + (isLast ? "┘" : "┴");
            }
            return string.Join(Environment.NewLine, s1, s2, s3, s4, s5);
        }

        private void SolutionExplorer_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(OnClosed != null)
            {
                OnClosed();
            }
        }
    }
}
