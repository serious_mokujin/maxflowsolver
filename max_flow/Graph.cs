﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace max_flow
{
    public class Graph
    {
        public const int k_infiniteWeight = int.MaxValue;

        protected int[][] weights;

        public int NumOfVertices { get; protected set; }

        public int this[int row, int col]
        {
            get { return weights[row][col]; }
        }

        public bool IsArcExists(int from, int to)
        {
            return weights[from][to] != k_infiniteWeight;
        }
    }
}
