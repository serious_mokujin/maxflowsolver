﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace max_flow
{
    public enum StepAction
    {
        Initialization,
        StartMarking,
        MarkVertice,
        StockMarked,
        StartTraceBack,
        TraceBackStep,
        TraceBackFinish,
        FindOutMinimalCut,
    }

    public enum SolutionResult
    {
        None,
        Success,
        CantFindST
    }

    public class Solution
    {
        public List<StepSlice> m_steps;
        public int S, T;
        public int[,] final_flow_state;
        public Tuple<int, int>[] min_cut;
        public int maxFlow;
    }
    public struct VertexMarkEvent
    {
        public int justMarkedVertice;
    }
    public struct FlowChangeEvent
    {
        public int arcV1;
        public int arcV2;
        public int oldFlow;
        public int newFlow;
    }

    public struct StepSlice
    {
        public StepAction action;
        public VertexMark[] marks_state;
        public int[,] flow_state;
        public VertexMarkEvent vertexMark;
        public FlowChangeEvent flowChange;
    }
    public struct VertexMark
    {
        public int sign;
        public int mark;
        public int flowDif;

        public void Reset()
        {
            sign = 0;
            mark = int.MaxValue;
            flowDif = int.MaxValue;
        }

        public void SetStart()
        {
            this.sign = -1;
            this.mark = -1;
            this.flowDif = int.MaxValue;
        }

        public void Set(int linkToVeritce, int sign, int flowDifference)
        {
            this.sign = sign;
            this.mark = linkToVeritce;
            this.flowDif = flowDifference;
        }

        public bool IsValid()
        {
            return mark != int.MaxValue;
        }

        public static VertexMark Def()
        {
            VertexMark m = new VertexMark();
            m.Reset();
            return m;
        }
    }

    public class MaxFlowSolver
    {
        private VertexMark[] marks;
        private int[,] flow;
        Graph m_graph;

        Solution solution;

        public Solution Solution
        {
            get { return solution; }
        }

        public MaxFlowSolver(Graph g)
        {
            m_graph = g;
        }

        public void Reset()
        {
            solution = new Solution();
            solution.m_steps = new List<StepSlice>();
            marks = new VertexMark[m_graph.NumOfVertices].Populate(VertexMark.Def());
            flow = new int[m_graph.NumOfVertices, m_graph.NumOfVertices].Populate(0);
        }

        private void TakeSlice(StepAction action)
        {
            TakeSlice(action, new VertexMarkEvent(), new FlowChangeEvent());
        }
        private void TakeSlice(StepAction action, VertexMarkEvent vm)
        {
            TakeSlice(action, vm, new FlowChangeEvent());
        }
        private void TakeSlice(StepAction action, FlowChangeEvent fc)
        {
            TakeSlice(action, new VertexMarkEvent(), fc);
        }
        private void TakeSlice(StepAction action, VertexMarkEvent vm, FlowChangeEvent fc)
        {
            StepSlice s = new StepSlice();
            s.action = action;
            s.flow_state = (int[,])flow.Clone();
            s.marks_state = (VertexMark[])marks.Clone();
            s.vertexMark = vm;
            s.flowChange = fc;
            solution.m_steps.Add(s);
        }

        private void FindST(out int S, out int T)
        {
            S = -1;
            T = -1;
            for (int v = 0; v < m_graph.NumOfVertices; v++)
            {
                if (S != -1 && T != -1)
                {
                    break;
                }

                if (S == -1)
                {
                    S = v;
                    for (int r = 0; r < m_graph.NumOfVertices; r++)
                    {
                        if (m_graph[r, v] != Graph.k_infiniteWeight)
                        {
                            S = -1;
                            break;
                        }
                    }
                }
                if (T == -1)
                {
                    T = v;
                    for (int c = 0; c < m_graph.NumOfVertices; c++)
                    {
                        if (m_graph[v, c] != Graph.k_infiniteWeight)
                        {
                            T = -1;
                            break;
                        }
                    }
                }
            }
        }

        private bool MarkVertices(int S, int T)
        {
            List<int> verticesToConcider = new List<int>();
            verticesToConcider.Add(S);
            marks[S].SetStart();

            TakeSlice(StepAction.StartMarking);

            while (verticesToConcider.Count > 0)
            {
                int currentVeritce = verticesToConcider[0];
                verticesToConcider.RemoveAt(0);

                for (int v = 0; v < m_graph.NumOfVertices; v++)
                {
                    if (!marks[v].IsValid())
                    {
                        if (m_graph.IsArcExists(currentVeritce, v))
                        {
                            if (flow[currentVeritce, v] < m_graph[currentVeritce, v])
                            {
                                marks[v].Set(currentVeritce, +1, Math.Min(marks[currentVeritce].flowDif, m_graph[currentVeritce, v] - flow[currentVeritce, v]));
                                verticesToConcider.Add(v);
                                TakeSlice(StepAction.MarkVertice, new VertexMarkEvent() { justMarkedVertice = v });
                                if (v == T)
                                {
                                    TakeSlice(StepAction.StockMarked, new VertexMarkEvent() { justMarkedVertice = v });
                                    return true;
                                }
                            }

                        }
                        else if (m_graph.IsArcExists(v, currentVeritce))
                        {
                            if (flow[v, currentVeritce] > 0)
                            {
                                marks[v].Set(currentVeritce, -1, Math.Min(marks[currentVeritce].flowDif, flow[v, currentVeritce]));
                                verticesToConcider.Add(v);
                                TakeSlice(StepAction.MarkVertice, new VertexMarkEvent() { justMarkedVertice = v });
                                if (v == T)
                                {
                                    TakeSlice(StepAction.StockMarked, new VertexMarkEvent() { justMarkedVertice = v });
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
            
        private void TraceBack(int S, int T)
        {
            TakeSlice(StepAction.StartTraceBack);
            int diff = marks[T].flowDif;
            int currentVertice = T;
            while (currentVertice != S)
            {
                if (marks[currentVertice].IsValid())
                {
                    int oldFlow = flow[marks[currentVertice].mark, currentVertice];
                    int newFlow = flow[marks[currentVertice].mark, currentVertice] + marks[currentVertice].sign * diff;
                    flow[marks[currentVertice].mark, currentVertice] = newFlow;
                    TakeSlice(StepAction.TraceBackStep, new FlowChangeEvent()
                    {
                        arcV1 = marks[currentVertice].mark,
                        arcV2 = currentVertice,
                        oldFlow = oldFlow,
                        newFlow = newFlow
                    });
                    currentVertice = marks[currentVertice].mark;
                }
            }
            marks.Populate(VertexMark.Def());
            TakeSlice(StepAction.TraceBackFinish);
        }

        public SolutionResult Solve()
        {
            Reset();
            TakeSlice(StepAction.Initialization);
            int S, T;
            FindST(out S, out T);

            if(S == -1 || T == -1 || S == T)
            {
                return SolutionResult.CantFindST;
            }

            solution.S = S;
            solution.T = T;

            do
            {
                bool stockMarked = MarkVertices(S, T);
                if (stockMarked)
                {
                    TraceBack(S, T);
                }
                else
                {
                    List<Tuple<int, int>> minCut = new List<Tuple<int, int>>();
                    for(int v = 0; v < m_graph.NumOfVertices; v++)
                    {
                        if(marks[v].IsValid())
                        {
                            for (int v2 = 0; v2 < m_graph.NumOfVertices; v2++)
                            {
                                if (!marks[v2].IsValid())
                                {
                                    if(m_graph.IsArcExists(v, v2))
                                    {
                                        minCut.Add(new Tuple<int, int>(v, v2));
                                    }
                                    else if(m_graph.IsArcExists(v2, v))
                                    {
                                        minCut.Add(new Tuple<int, int>(v2, v));
                                    }
                                }
                            }
                        }
                    }
                    solution.min_cut = minCut.ToArray();
                    solution.final_flow_state = (int[,])flow.Clone();
                    TakeSlice(StepAction.FindOutMinimalCut);

                    int resultFlow = 0;
                    for (int k = 0; k < m_graph.NumOfVertices; k++)
                    {
                        if (m_graph.IsArcExists(S, k))
                        {
                            resultFlow += flow[S, k];
                        }
                    }
                    solution.maxFlow = resultFlow;
                    return SolutionResult.Success;
                }
            } while (true);
        }
    }
}
